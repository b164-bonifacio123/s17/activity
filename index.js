console.log("Hello World")


//#3. Create an addStudent() function that will accept a name of the student and add it to the student array.
let student = []

function addStudent(name) {
	student.push(name);
	console.log(`${name} + was added to the student's list.`)
}

//#4. Create a countStudents() function that will print the total number of students in the array.
function countStudent() {
	console.log(`There are total of ${students.length} students enrolled`)
}


//#5. Create a printStudents() function that will sort and individually print the students (sort and forEach methods) in the array.
function printStudent() {
	student.sort().forEach(function(student) {
		console.log(student)
	})
}


//#6. Create a findStudent() function that will do the following:
//Search for a student name when a keyword is given (filter method).
//If one match is found print the message studentName is an enrollee.
//If multiple matches are found print the message studentNames are enrollees.
//If no match is found print the message studentName is not an enrollee.
//The keyword given should not be case sensitive.
function findStudent(keyword) {

	let match = students.filter(function(student) {
		return student.toLowerCase().includes(keyword.toLowerCase())
	})

	if(match.length == 1){
		console.log( `${match} is an Enrollee`);
	} else if(match.length > 1){
		console.log(`${match} are Enrollees`);
	} else {
		console.loge(`No student found with the name ${keyword}`)
	}

}

//Stretch Goals:
//1. Add a section to the students
//2. Remove the name of the students

function addSection(section) {
	let sectionedStudents = students.map(function(student) {
		//returns the name of the student plus the added section

		return student + ' - section' + section;
	})

	//Prints the array of sectionedStudents for verification
	console.log(sectionedStudents);
}

function removedStudent(name) {
	//converts the first letter of the name to uppercase
	let firstLetter = name.slice(0, 1).toUpperCase();
	//Retrieves all other letters of the name
	let remainingLetters = name.slice(1, name.length);

	let studentName = firstLetter + remainingLetters;

	//retrieve the index of the name provided
	let studentIndex = students.indexOf(studentName);

	//if a match is found, the index number would be 0 or greater than
	//if the name is not found the result is -1
	if(studentIndex >= 0) {
		//to remove the index of the name provided
		students.splice(studentIndex, 1)
		console.log(name + "was removed from the student's list")
	} else{
		console.log("no student found")
	}
}












